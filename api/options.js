module.exports = function *() {
    var ret = {
        energies: (yield this.knex.raw(
            `SELECT DISTINCT jsonb_array_elements(data->'colors') AS color
            FROM cards
            WHERE jsonb_array_length(data->'colors') > 0`
        )).rows.map(function (el) {
                return el.color;
            }),

        basicTypes: (yield this.knex.raw(
            `SELECT DISTINCT data->>'basicType' AS type
            FROM cards`
        )).rows.map(function (el) {
                return el.type;
            }),

        specificTypes: (yield this.knex.raw(
            `SELECT data->>'basicType' as group, json_agg(DISTINCT data->>'specificType') as options
             FROM cards
             WHERE (data->>'specificType') IS NOT NULL
             GROUP BY data->>'basicType'`
        )).rows,

        sets: (yield this.knex.raw(
            `SELECT name
            FROM expansions
            ORDER BY date DESC`
        )).rows.map(function (el) {
                return el.name;
            })
    };

    console.log(ret);

    this.body = ret;
};