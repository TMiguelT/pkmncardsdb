const PAGE_SIZE = 20;

module.exports = function *() {
    var knex = this.knex;
    var params = this.request.query;
    var page = "page" in params ? params.page : 1;
    var filters = "filters" in params ? JSON.parse(params.filters) : [];
    var query = this.knex("cards")
        .select(knex.raw(`
            data->'smallImg' AS "smallImg",
            data->'largeImg' AS "largeImg",
            data->'largeImg' AS id`
        ))
        .joinRaw(`JOIN expansions ON expansions.name = data->>'set'`)
        .offset((page - 1) * PAGE_SIZE)
        .limit(PAGE_SIZE);

    var self = this;

    //If they have a query, filter the results
    filters.forEach(function (filter) {
        switch (filter.type) {

            case "randomize":
                query.orderByRaw('random()');
                break;

            case "fromSet":
                query = query.whereRaw(`data->>'set' ILIKE ANY(?::text[])`, [filter.value]);
                break;

            case "expandedLegal":
                //Select the date of Black and White
                var subquery = knex('expansions')
                    .select('date')
                    .where({name: 'Black & White'});

                query = query.where("expansions.date", ">=", subquery);
                break;

            case "afterSet":
                //Select the date of the specified set
                var subquery = knex('expansions')
                    .select('date')
                    .whereRaw(`name ILIKE ANY(?::text[])`, [filter.value]);

                query = query.where("expansions.date", ">=", subquery);
                break;

            case "dmgLt":
                query = query.whereRaw(`? > ANY (SELECT (el->>'damage')::integer FROM jsonb_array_elements(data->'attacks') el)`, filter.value);
                break;

            case "dmgGt":
                query = query.whereRaw(`? < ANY (SELECT (el->>'damage')::integer FROM jsonb_array_elements(data->'attacks') el)`, filter.value);
                break;

            case "noEx":
                query = query.whereRaw(`data->>'name' NOT ILIKE '%EX%'`);
                break;

            case "cardName":
                query = query.whereRaw(`data->>'name' ILIKE '%' || ? || '%'`, [filter.value]);
                break;

            case "removeDuplicates":
                query = query.select(self.knex.raw(
                    `DISTINCT ON (data->'name', data->'abilities', data->'attacks')
                    "data"`));
                break;

            case "basicType":
                query = query.whereRaw(`data->>'basicType' = ANY(?)`, [filter.value]);
                break;

            case "specificType":
                query = query.whereRaw(`data->>'specificType' = ANY(?)`, [filter.value]);
                break;

            case "cardTextContains":
                query = query.whereRaw(
                    `concat(data::text) ILIKE concat('%', ?::text, '%') `, [filter.value]);
                break;

            case "attackTextContains":
                query = query.whereRaw(
                    `data->>'attacks' ILIKE '%' || ? || '%' `, [filter.value]);
                break;

            case "hasAbility":
                query = query.whereRaw(`jsonb_array_length(data->'abilities') > 0`);
                break;

            case "hasTrait":
                query = query.whereRaw(`jsonb_array_length(data->'traits') > 0`);
                break;

            case "hasRules":
                query = query.whereRaw(`jsonb_array_length(data->'rules') > 0`);
                break;

            case "hasEvolution":
                query.whereRaw(`data->>'name' IN (SELECT data->>'evolvesFrom' FROM cards)`);
                break;

            //case "evolves":
            //    query = query.whereRaw(
            //        `? = ANY (
            //        SELECT pre_cards.data->>'name'
            //        FROM
            //            pre_evolutions pre
            //            JOIN cards pre_cards ON pre.pre_evo_id = pre_cards.id
            //        WHERE pre.id = cards.id
            //
            //        UNION
            //
            //        SELECT post_cards.data->>'name'
            //        FROM
            //            pre_evolutions post
            //            JOIN cards post_cards ON post.id = post_cards.id
            //        WHERE post.pre_evo_id = cards.id
            //    )`, filter.value);
            //
            //    break;

            case "resistance":
                //Find intersection between the input array and the types array
                query = query.whereRaw(
                    `?::text[] &&
                    (
                        SELECT array_agg(value::text)
                        FROM jsonb_array_elements_text(data->'resistance'->'types')
                    )`, [filter.value]);
                break;
            case "weakness":
                //Find intersection between the input array and the types array
                query = query.whereRaw(
                    `?::text[] &&
                    (
                        SELECT array_agg(value::text)
                        FROM jsonb_array_elements_text(data->'weakness'->'types')
                    )`, [filter.value]);
                break;

            case "abilityContains":
                query = query.whereRaw(`data->>'abilities' ILIKE '%' || ? || '%'`, [filter.value]);
                break;

            case "traitContains":
                query = query.whereRaw(`data->>'traits' ILIKE '%' || ? || '%'`, [filter.value]);
                break;

            case "colour":
                query = query.whereRaw(`data->>'colors' ILIKE '%' || ? || '%'`, filter.value);
                break;

            case "hpGt":
                query = query.whereRaw(`(data->>'hp')::integer > ?`, [filter.value]);
                break;

            case "hpLt":
                query = query.whereRaw(`(data->>'hp')::integer < ?`, [filter.value]);
                break;

            case "rcGt":
                query = query.whereRaw(`(data->>'retreatCost')::integer > ?`, [filter.value]);
                break;

            case "rcLt":
                query = query.whereRaw(`(data->>'retreatCost')::integer < ?`, [filter.value]);
                break;
        }
    });

    var cards = yield query;

    console.log(query.toSQL());

    if (cards.length <= 0)
        this.body = false;
    else
        this.body = cards;
};