var _ = require("lodash");

var types = [
    {
        groupName: "Miscellaneous",
        items: [
            //{
            //    name: "Remove Duplicates",
            //    value: "removeDuplicates"
            //},
            {
                name: "Randomize",
                value: "randomize"
            }
        ]
    },
    {
        groupName: "Abilities and Traits",
        items: [
            {
                name: "Has Ability",
                value: "hasAbility"
            },
            {
                name: "Has Ancient Trait",
                value: "hasTrait"
            },
            {
                name: "Ability Text",
                value: "abilityContains",
                hint: "(contains the following text)"
            },
            {
                name: "Ancient Trait Text",
                value: "traitContains",
                hint: "(contains the following text)"
            }
        ]
    },
    {
        groupName: "Attacks",
        items: [
            {
                name: "Attack Text",
                value: "attackTextContains",
                hint: "(contains the following text)"
            },
            {
                name: "Damage Greater than",
                value: "dmgGt",
                hint: "(has one attack with a base damage greater than)"
            },
            {
                name: "Damage Less than",
                value: "dmgLt",
                hint: "(has one attack with a base damage greater than)"
            }
        ]
    },
    {
        groupName: "Card",
        items: [
            {
                name: "Card Name",
                value: "cardName",
                hint: "(contains the following text)"
            },
            {
                name: "Card Text",
                value: "cardTextContains",
                hint: "(contains the following text)"
            }
        ]
    },
    {
        groupName: "Set",
        items: [
            {
                name: "From Set",
                value: "fromSet"
            },
            {
                name: "From or After Set",
                value: "afterSet"
            },
            {
                name: "Legal in Expanded",
                value: "expandedLegal"
            }
        ]
    },
    {
        groupName: "Weakness and Resistance",
        items: [
            {
                name: "Resistant To",
                value: "resistance"
            },
            {
                name: "Weak To",
                value: "weakness"
            }
        ]
    },
    {
        groupName: "Type",
        items: [
            {
                name: "Colour",
                value: "colour",
                hint: "(is one of)"
            },
            {
                name: "Basic Type",
                value: "basicType",
                hint: "(is one of)"
            },
            {
                name: "Specific Type",
                value: "specificType",
                hint: "(is one of)"
            },
            {
                name: "Has an Evolution",
                value: "hasEvolution"
            },
            {
                name: "No EXs",
                value: "noEx"
            }
        ]
    },
    {
        groupName: "Stats",
        items: [
            {
                name: "HP Greater Than",
                value: "hpGt"
            },
            {
                name: "HP Less Than",
                value: "hpLt"
            },
            {
                name: "Retreat Cost Less Than",
                value: "rcLt"
            },
            {
                name: "Retreat Cost More Than",
                value: "rcGt"
            }
        ]
    }

];

var map = _.chain(types).map("items").flatten().indexBy("value").value();
var length =  _.size(map) + _.size(types);

module.exports = {
    array: types,
    map: map,
    number: length
};