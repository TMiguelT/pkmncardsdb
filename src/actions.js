var Reflux = require('reflux');
var assign = require('lodash.assign');
var request = require("superagent");

var simpleActions = Reflux.createActions([
    "addFilter",
    "editFilter",
    "removeFilter",
    "clearFilters",
    "pageScroll",
    "readyToSubmit"
]);

var complexActions = Reflux.createActions({
    query: {children: ["completed", "failed"]},
    requestOptions: {children: ["completed", "failed"]}
});

//On request options, access the API
complexActions.requestOptions.listen(function () {
    var self = this;

    request
        .get('/api/options')
        .end(function (err, res) {
            if (err)
                self.failed(err);
            else
                self.completed(res.body)
        });
});

// On a query event, access the API
complexActions.query.listen(function (page) {
    var self = this;

    //Load page one if none specified, otherwise go to the next page
    var pageNum = page ? page : 1;
    var filters = JSON.stringify(require("./stores").filterStore.filters);

    request
        .get('/api/query')
        .query({page: pageNum, filters: filters})
        .end(function (err, res) {
            if (err)
                self.failed(err);
            else
                self.completed(res.body, pageNum)
        });
});


module.exports = assign(
    {},
    simpleActions,
    complexActions
);