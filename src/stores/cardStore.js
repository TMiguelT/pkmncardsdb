var Reflux = require('reflux');
var actions = require("../actions");

var cardStore = Reflux.createStore({
    listenables: [actions],

    onReadyToSubmit: function () {
        this.hasMore = true;
        this.triggerState();
    },

    onPageScroll: function () {
        //If the user scrolls the page, if we're not already loading data, send a query for the next page
        if (!this.loading && this.hasMore) {
            actions.query(this.page + 1);
        }
    },
    onQuery: function (page) {
        if (page == 1)
            this.hasMore = true;
        this.loading = true;
        this.triggerState();
        //console.log("Query made");
    },
    onQueryCompleted: function (data, page) {

        this.loading = false;

        //If the query returned false, we're done
        if (data == false) {
            this.hasMore = false;

            //If this was the first query, clear the result because it failed
            if (page <= 1)
                this.cards = [];

            this.triggerState();
        }

        //Otherwise, we need to add cards
        else {
            //If it's a new query, wipe the current results
            if (page <= 1)
                this.cards = data;
            else
                this.cards = this.cards.concat(data);

            this.page = page;
            this.triggerState();
            //console.log("Query suceeded");
        }
    },
    onQueryFailed: function () {
        this.loading = false;
        this.triggerState();
        //console.log("Query failed");
    },
    getInitialState: function () {
        this.cards = [];
        this.loading = false;
        this.page = 1;

        //True when all cards from the query have been loaded
        this.hasMore = false;

        return this.getState();
    },
    getState: function () {
        return {
            cards: this.cards,
            page: this.page,
            loading: this.loading,
            hasMore: this.hasMore
        };
    },
    triggerState: function () {
        this.trigger(this.getState());
    }
});

module.exports = cardStore;