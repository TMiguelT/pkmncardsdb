var Reflux = require('reflux');
var actions = require("../actions");
var _ = require("lodash");

var cardStore = Reflux.createStore({
    listenables: [actions],

    onRequestOptionsCompleted: function (data) {
        console.log("Options returned");
        _.assign(this, data);
        this.triggerState();
    },
    getInitialState: function () {
        this.energies = [];
        this.basicTypes = [];
        this.specificTypes = [];
        this.sets = [];

        return this.getState();
    },
    getState: function () {
        return {
            energies: this.energies,
            basicTypes: this.basicTypes,
            specificTypes: this.specificTypes,
            sets: this.sets
        };
    },
    triggerState: function () {
        this.trigger(this.getState());
    }
});

module.exports = cardStore;