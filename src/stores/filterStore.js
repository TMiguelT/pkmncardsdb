var Reflux = require('reflux');
var actions = require("../actions");
var _ = require("lodash");
var URI = require('URIjs');
var rison = require('rison');

var currentId = 0;

var filterStore = Reflux.createStore({
    listenables: [actions],
    onAddFilter: function (type) {
        this.filters.push({
            id: currentId++,
            type: type,
            value: ""
        });
        this.trigger(this.filters);
    },
    onRemoveFilter: function (id) {
        _.remove(this.filters, function (filter) {
            return filter.id === id;
        });
        this.trigger(this.filters);
    },
    onEditFilter: function (id, value) {
        var filter = _.chain(this.filters).filter(function (filter) {
            return filter.id === id;
        }).first().value();

        filter.value = value;
        this.trigger(this.filters);
    },
    onClearFilters: function () {
        this.filters = [];
        this.trigger(this.filters);
    },
    onQuery: function () {
        console.log("Load");
    },
    onQueryCompleted: function () {
        console.log("Completed load");
    },
    onQueryFailed: function () {
        console.log("Error during load");
    },
    getInitialState: function () {

        //If the URL has a search term, use it, and run the search
        var uri = new URI();
        var search = uri.search(true);
        if ('q' in search) {

            //Add IDs to each filter 
            var input = rison.decode_array(search.q);
            _.forEach(input, function (filter) {
                filter.id = currentId++;
            });
            this.filters = input;
            actions.query(1);
        }

        //Otherwise use an empty filter list
        else
            this.filters = [];

        return this.filters;
    }
});

//Whenever the filterStore changes, update the URL
filterStore.listen(function (status) {

    if (Array.isArray(status)) {

        var url;
        if (status.length > 0) {

            //Remove the IDs from each filter
            var filtered = _.map(status, function (filter) {
                return _.omit(filter, 'id');
            });

            //Set the URL to the RISON encoded version of this
            url = location.protocol + '//' + location.host + location.pathname + "?q=" + rison.encode_array(filtered);
        }
        else
            url = location.protocol + '//' + location.host + location.pathname;

        history.pushState(status, null, url);
    }
});

module.exports = filterStore;