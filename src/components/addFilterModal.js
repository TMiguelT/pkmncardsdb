var React = require('react/addons');
var Bootstrap = require('react-bootstrap');
var Filter = require("./filter.jsx");

var actions = require("../actions");

var AddFilter = React.createClass({
    mixins: [React.addons.LinkedStateMixin],

    getInitialState: function () {
        return {
            filter: "cardName"
        };
    },

    handleAdd: function () {
        actions.addFilter(this.state.filter);
        this.props.onRequestHide();
    },

    render: function () {
        return (
            <Bootstrap.Modal
                {...this.props}
                title='Add filter'
                backdrop={true}
                animation={true}>
                <div className='modal-body'>
                    <Bootstrap.Input
                        valueLink={this.linkState('filter')}
                        type='select'
                        label='Filter Type'
                        size={this.props.filterNumber}
                        placeholder='filter'>{
                        this.props.filterTypes.map(function (filterGroup) {
                            return (
                                <optgroup key={filterGroup.groupName} label={filterGroup.groupName}>{
                                    filterGroup.items.map(function (filter) {
                                        return <option key={filter.value} value={filter.value}>{filter.name}</option>
                                    })
                                }</optgroup>
                            );
                        })
                    }
                    </Bootstrap.Input>
                </div>
                <div className='modal-footer'>
                    <Bootstrap.Button onClick={this.props.onRequestHide}>Cancel</Bootstrap.Button>
                    <Bootstrap.Button onClick={this.handleAdd} bsStyle='primary'>Add</Bootstrap.Button>
                </div>
            </Bootstrap.Modal>
        );
    }
});

module.exports = AddFilter;

