//Libraries
var Reflux = require('reflux');
var React = require('react');

//App settings
Reflux.setPromise(require('bluebird'));
var actions = require("./actions");
var stores = require("./stores");

//Components
var Cards = require("./components/cards.jsx");
var Filters = require("./components/filters.jsx");
var Bootstrap = require('react-bootstrap');
var AddFilter = require("./components/addFilterModal");
var LaddaButton = require('react-ladda');

//Filters, a constant set of options for the query
var filterTypes = require("./presetData/filterTypes").array;

//Preload the pokemon card back
var img = new Image();
img.onload = function () {
    actions.readyToSubmit();
};
img.src = "http://assets7.pokemon.com/assets/cms2/img/cards/web/cardback.png";

//App entry point
var App = React.createClass({
    componentWillMount: function () {

        //On application load, get the filter options
        actions.requestOptions();
    },

    mixins: [
        Reflux.connect(stores.cardStore, "cardData"),
        Reflux.connect(stores.filterStore, "filters"),
        Reflux.connect(stores.optionStore, "options")
    ],

    handleSubmit: function () {
        actions.query(1);
    },

    render: function () {
        return (
            <div style={{textAlign: "center"}}>
                {/*Admin stuff*/}
                <div style={{textAlign: "center", margin:"20px 20px"}}>
                    <Filters filters={this.state.filters} options={this.state.options}/>
                    <Bootstrap.ButtonToolbar style={{textAlign: "center"}} className="center-block">
                        <LaddaButton style="expand-right" active={this.state.cardData.loading}>

                            <Bootstrap.Button bsStyle='primary' bsSize='large'
                                              onClick={this.handleSubmit}>Search</Bootstrap.Button>
                        </LaddaButton>
                    </Bootstrap.ButtonToolbar>
                </div>

                {/*Separator*/}
                <br/>

                {/*Actual data*/}
                <Cards hasMore={this.state.cardData.hasMore} cards={this.state.cardData.cards}/>
            </div>
        );
    }
});

React.render(<App />, document.body);