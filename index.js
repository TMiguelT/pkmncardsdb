var app = require('koa')();
var mount = require('koa-mount');
var send = require('koa-send');
var router = require('koa-router');
var knex = require('koa-knex');
var config = require("./connection").config;

app
    //compress everything
    .use(require('koa-gzip')())

    //Log each request
    .use(require('koa-logger')())

    .use(knex(config))

    //Use the router
    .use(router(app))

    .get("/api/query", require("./api/query"))

    .get("/api/options", require("./api/options"))

    //Mount the static file server
    .use(mount("/public", require('koa-static')("dist")))

    //When the user goes anywhere that's not the api or public, send them the main page send the app
    .use(function *() {
        yield send(this, "./index.html", {root: __dirname});
    });

var port = process.env.PORT || 3000;
app.listen(port);
console.log(process.env.DATABASE_URL);
console.log("Listening at port " + port);