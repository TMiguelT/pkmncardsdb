var knex = require("../connection").knex;

var createQuery =
    `CREATE TABLE IF NOT EXISTS cards(
        data jsonb,
        id TEXT
    );`;
var createSetQuery =
    `CREATE TABLE IF NOT EXISTS expansions(
        name TEXT PRIMARY KEY,
        date TIMESTAMP
    );`;
module.exports = function* () {
    yield knex.raw(createQuery);
    yield knex.raw(createSetQuery);
    knex.destroy();
};