var knex = require("../connection").knex;
var scraper = require("../../PkmnCardsScraper/index");
var request = require('request-promise');
var cheerio = require("cheerio");
var _ = require('lodash');
var trim = require("trim");

const SCRAPE_URL = "http://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_Trading_Card_Game_expansions";

function nodeText(node, $){
    return trim($(node).text());
}

module.exports = function* () {
    var $ = cheerio.load(yield request(SCRAPE_URL));
    var rows = $("table.sortable tr");
    var sets = _(rows)
        .filter(function (row) {
            var cells = $(row).find("td");
            return Date.parse($(cells[6]).text());
        }).map(function (row) {
            var cells = $(row).find("td");
            return {
                name: nodeText(cells[2], $),
                date: new Date(nodeText(cells[6], $))
            };
        })
        .unique("name")
        .value();

    yield knex("expansions").insert(sets);
};