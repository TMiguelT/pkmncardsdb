var knex = require("../connection").knex;
var scraper = require("../../PkmnCardsScraper/index");

module.exports = function* () {
    var cards = yield scraper.scrapeAll();
    var hashes = cards.map(function (card) {
        return {
            data: card,
            id: card.set + ' ' + card.setId
        };
    });
    yield knex("cards").insert(hashes);
    knex.destroy();
};