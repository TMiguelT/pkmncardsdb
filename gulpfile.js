var gulp = require('gulp');
var Promise = require('bluebird');
var watchify = require('watchify');
var reactify = require('reactify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');
var uglifyify = require('uglifyify');

gulp.task("scrape", function () {
    var scrape = require("./tasks/scrape");
    return Promise.coroutine(scrape)();
});

gulp.task("scrapeSets", function () {
    var scrape = require("./tasks/scrapeSets");
    return Promise.coroutine(scrape)();
});

gulp.task("buildDb", function () {
    var build = require("./tasks/buildDb");
    return Promise.coroutine(build)();
});

gulp.task("dist", ["buildDb", "build"]);

gulp.task("build", ["css", "js"]);

gulp.task("css", function () {
    gulp.src("css/*")
        .pipe(gulp.dest("./dist"));
});

gulp.task('scripts', function() {
    return scripts(false);
});

gulp.task('watchScripts', function() {
    return scripts(true);
});

function scripts(debug) {
    var bundler, rebundle;
    bundler = browserify('./src/app.jsx', {
        basedir: __dirname,
        debug: debug,
        cache: {}, // required for watchify
        packageCache: {}, // required for watchify
        fullPaths: debug // required to be true only for watchify
    });

    if(debug)
        bundler = watchify(bundler);
    else
        bundler.transform({
            global: true
        }, 'uglifyify');

    bundler.transform(reactify);

    rebundle = function() {
        var stream = bundler.bundle();
        //stream.on('error', handleError('Browserify'));
        stream = stream.pipe(source('bundle.js'));
        return stream.pipe(gulp.dest('./dist'));
    };

    bundler.on('update', rebundle);
    return rebundle();
}
