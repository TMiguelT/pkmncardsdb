SELECT cards.data->'name', json_agg(evo.data->'name') 
FROM 
	cards
	JOIN card_evolutions link ON cards.id = link.id
	JOIN cards evo ON evo.id = link.evolution
GROUP BY cards.id