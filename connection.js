var parse = require('pg-connection-string').parse;
var connection;

if ("DATABASE_URL" in process.env)
    connection = parse(process.env.DATABASE_URL);
else
    connection = {
        port: 5433,
        host: 'localhost',
        user: 'postgres',
        password: 'Proline185',
        database: 'pkmncards'
    };

var config = {
    client: 'pg',
    connection: connection
};

module.exports = {
    config: config,
    knex: require("knex")(config)
};